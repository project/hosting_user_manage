<?php
/**
 * @file
 *   Expose the extra tasks feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_user_manage_hosting_feature() {
  $features['user_add'] = array(
    'title' => t('User add'),
    'description' => t("Tasks that expose additional Drush commands to Aegir's frontend."),
    'status' => HOSTING_FEATURE_ENABLED,
    'module' => 'hosting_user_manage',
    'group' => 'advanced',
  );

  $features['user_cancel'] = array(
    'title' => t('User cancel'),
    'description' => t("Tasks that expose additional Drush commands to Aegir's frontend."),
    'status' => HOSTING_FEATURE_ENABLED,
    'module' => 'hosting_user_manage',
    'group' => 'advanced',
  );

  $features['user_block'] = array(
    'title' => t('User block'),
    'description' => t("Tasks that expose additional Drush commands to Aegir's frontend."),
    'status' => HOSTING_FEATURE_ENABLED,
    'module' => 'hosting_user_manage',
    'group' => 'advanced',
  );

  $features['user_unblock'] = array(
    'title' => t('User unblock'),
    'description' => t("Tasks that expose additional Drush commands to Aegir's frontend."),
    'status' => HOSTING_FEATURE_ENABLED,
    'module' => 'hosting_user_manage',
    'group' => 'advanced',
  );    

  return $features;
}

