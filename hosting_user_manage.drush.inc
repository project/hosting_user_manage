<?php

function drush_hosting_user_manage_pre_hosting_task($task) {
  $task = &drush_get_context('HOSTING_TASK');
  if ($task->ref->type == 'site' && $task->task_type == 'user_add') {
  	$task->args[1] = $task->task_args['name'];
  	$task->options['mail'] = $task->task_args['mail'];
  	$task->options['password'] = $task->task_args['password'];
  }

  if ($task->ref->type == 'site' && $task->task_type == 'user_cancel') {
  	$task->args[1] = $task->task_args['name'];
  }

  if ($task->ref->type == 'site' && $task->task_type == 'user_block') {
  	$task->args[1] = $task->task_args['name'];
  }

  if ($task->ref->type == 'site' && $task->task_type == 'user_unblock') {
    $task->args[1] = $task->task_args['name'];
  }   
}
