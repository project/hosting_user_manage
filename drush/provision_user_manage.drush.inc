<?php
/**
 * @file
 * Provision/Drush hooks for the provision_user_manage module.
 *
 * These are the hooks that will be executed by the drush_invoke function.
 */

/**
 * Implementation of hook_drush_command().
 */
function provision_user_manage_drush_command() {
  $items['provision-user_add'] = array(
    'description' => 'Add user to multiple sites',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'name' => dt('The name of the account to add'),
    ),
    'options' => array(
      'mail' => dt('The email address for the new account'),
      'password' => dt('The password for the new account'),
    ),    
  );

  $items['provision-user_cancel'] = array(
    'description' => 'Cancel user from multiple sites',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'name' => dt('The name of the account to cancel'),
    ),   
  );

  $items['provision-user_block'] = array(
    'description' => 'Block users from multiple sites',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'name' => dt('A comma delimited list of uids, user names, or email addresses.'),
    ),   
  );

  $items['provision-user_unblock'] = array(
    'description' => 'Unblock users from multiple sites',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'name' => dt('A comma delimited list of uids, user names, or email addresses.'),
    ),   
  );  

  return $items;
}

/**
 * Implements the provision-user_add command.
 */
function drush_provision_user_manage_provision_user_add($name) {
  $options = array();
  $options['mail'] = drush_get_option('mail', FALSE);
  $options['password'] = drush_get_option('password', FALSE);

  provision_backend_invoke(d()->name, 'user-create', array($name), $options);
  provision_backend_invoke(d()->name, 'user-add-role', array('administrator', $name));
  drush_log(dt('Drush user added : ' . $name));
}

/**
 * Implements the provision-user_cancel command.
 */
function drush_provision_user_manage_provision_user_cancel($name) {
  provision_backend_invoke(d()->name, 'user-cancel', array($name));
  drush_log(dt('Drush user cancelled : ' . $name));
}

/**
 * Implements the provision-user_block command.
 */
function drush_provision_user_manage_provision_user_block($name) {
  provision_backend_invoke(d()->name, 'user-block', array($name));
  drush_log(dt('Drush user blocked : ' . $name));
}

/**
 * Implements the provision-user_unblock command.
 */
function drush_provision_user_manage_provision_user_unblock($name) {
  provision_backend_invoke(d()->name, 'user-unblock', array($name));
  drush_log(dt('Drush user unblocked : ' . $name));
}